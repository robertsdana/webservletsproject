package WebServlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            String answer = "";
            String correctanswer = "";
            String alternateanswer = "";
            if (request.getParameterMap().containsKey("answer")) {
                answer = request.getParameter("answer");
                correctanswer = "spinach";
            } else if (request.getParameterMap().containsKey("bonus-answer")) {
                answer = request.getParameter("bonus-answer");
                correctanswer = "cross-site scripting";
                alternateanswer = "xss";
            }
            if (answer.toLowerCase().equals(correctanswer) || answer.toLowerCase().equals(alternateanswer)) {
                out.println("<h2>Correct!</h2>");
            } else {
                out.println("<h2>Incorrect!</h2>");
            }

            out.println("<p>You guessed " + answer + ", the correct answer was " + correctanswer + "<p>");
            if (request.getParameterMap().containsKey("bonus-answer")) {
                out.println("<p>An alternate answer was " + alternateanswer + "</p>");
            }
            if (request.getParameterMap().containsKey("answer") && answer.toLowerCase().equals(correctanswer)) {
                try {
                    RequestDispatcher view = request.getRequestDispatcher("BonusQuiz.html");
                    view.forward(request, response);
                } catch (Exception e) {
                    System.out.println("Something went wrong with the bonus question. Try again later?");
                    System.out.println(e);
                }
            }
        } catch (Exception e) {
            System.out.println("Something went wrong.");
            System.out.println(e);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Not available at this time.");
    }
}
